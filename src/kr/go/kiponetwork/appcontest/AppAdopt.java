package kr.go.kiponetwork.appcontest;

import java.util.ArrayList;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class AppAdopt extends SherlockFragment {
	
	ArrayList<MyItem> arItem;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.activity_main, container, false);
		//mList = (ListView) v.findViewById(R.id.listView1);
		//ImageView img = (ImageView) findViewBy
		//v.setBackground(@drawable/tent_10)
		return v;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		arItem = new ArrayList<MyItem>();
        MyItem mi;
        mi = new MyItem(R.drawable.k1, " 금상: 로앤비법률정보","https://play.google.com/store/apps/details?id=com.lawnb.lawinfo");arItem.add(mi);
        mi = new MyItem(R.drawable.k2, " 은상: 국가과학기술정보센터","https://play.google.com/store/apps/details?id=com.yagins.mobileNDSL");arItem.add(mi);
        mi = new MyItem(R.drawable.k3, " 은상: 정보통신용어사전","https://play.google.com/store/apps/details?id=com.trizcorp.kccword");arItem.add(mi);
        mi = new MyItem(R.drawable.k99, " 동상: 스마트 유럽여행","http://www.tstore.co.kr/userpoc/game/viewProduct.omp?insProdId=0000365038");arItem.add(mi);
        mi = new MyItem(R.drawable.k5, " 동상: 국가법령정보","https://play.google.com/store/apps/details?id=mobirus.korealaw");arItem.add(mi);
        mi = new MyItem(R.drawable.k6, " 동상: 구글 번역기","https://play.google.com/store/apps/details?id=com.google.android.apps.translate");arItem.add(mi);
        mi = new MyItem(R.drawable.k7, " 동상: 문+리더","https://play.google.com/store/apps/details?id=com.flyersoft.moonreader");arItem.add(mi);
        mi = new MyItem(R.drawable.k8, " 입선: CloudOn","https://play.google.com/store/apps/details?id=com.cloudon.client");arItem.add(mi);
        mi = new MyItem(R.drawable.k9, " 입선: KingSoft Office","https://play.google.com/store/apps/details?id=cn.wps.moffice_eng");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k10, " 입선: Send Anywhere","https://play.google.com/store/apps/details?id=com.estmob.android.sendanywhere");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k11, " 입선: 한컴뷰어","https://play.google.com/store/apps/details?id=kr.co.hancom.hancomviewer.androidmarket");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k12, " 입선: 스마트 목디스크","https://play.google.com/store/apps/details?id=com.rabbithands.GetUpNeckPro");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k13, " 입선: 국민건강체조","https://play.google.com/store/apps/details?id=com.dalbongs.kspo.gymnastics");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k14, " 입선: 체조송","https://play.google.com/store/apps/details?id=kr.or.kosha");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k15, " 입선: GPS Tracker","https://play.google.com/store/apps/details?id=demo.GPSTracker");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k16, " 입선: 책속의 한줄","https://play.google.com/store/apps/details?id=kr.co.ladybugs.booksns");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k17, " 입선: Patent an idea","https://play.google.com/store/apps/details?id=patent.an.idea");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k18, " 입선: Patent Flipper","https://play.google.com/store/apps/details?id=com.diarobo.PatentFlipperTrial");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k19, " 입선: 네이버 메모","https://play.google.com/store/apps/details?id=com.nhn.android.navermemo");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k20, " 입선: 네이버 캘린더","https://play.google.com/store/apps/details?id=com.nhn.android.calendar");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k21, " 입선: Wunderlist","https://play.google.com/store/apps/details?id=com.wunderkinder.wunderlistandroid");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k22, " 입선: 네이버 밴드","https://play.google.com/store/apps/details?id=com.nhn.android.band");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k23, " 입선: 지하철 종결자","https://play.google.com/store/apps/details?id=teamDoppelGanger.SmarterSubway");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k24, " 입선: 전국버스","https://play.google.com/store/apps/details?id=net.hyeongkyu.android.incheonBus");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k25, " 입선: 굿모님팝스 Pod Pro","https://play.google.com/store/apps/details?id=com.zetty.zettygmppro");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k26, " 입선: StarPlayer","https://play.google.com/store/apps/details?id=com.starapp.starplayer");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k27, " 입선: 키포아카데미","https://play.google.com/store/apps/details?id=com.fourcsoft.KipoAndroid");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k28, " 입선: 원클릭경매","https://play.google.com/store/apps/details?id=kr.co.peoplentech.OneClickAuction");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k29, " 입선: 긴급상황알림스마트","https://play.google.com/store/apps/details?id=com.dump.EmerCall");arItem.add(mi);

        
        
        
        	
        
        MyListAdapter MyAdapter = new MyListAdapter(getActivity(),R.layout.icon_text, arItem);
        ListView MyList;
        MyList = (ListView) getView().findViewById(R.id.list);
        MyList.setAdapter(MyAdapter);
        
	}

	   
    class MyItem {
    	MyItem(int aIcon, String aName, String aLink) {
    		Icon = aIcon;
    		Name = aName;
    		Link = aLink;
    	}
    	int Icon;
    	String Name;
    	String Link;
    }
    
    class MyListAdapter extends BaseAdapter {
    	Context maincon;
    	LayoutInflater Inflater;
    	ArrayList<MyItem> arSrc;
    	int layout;
    	
    	public MyListAdapter(Context context, int alayout, ArrayList<MyItem> aarSrc) {
    		maincon = context;
    		Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		arSrc = aarSrc;
    		layout = alayout;
    	}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arSrc.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arSrc.get(position).Name;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			//final int pos = position;
			
			if (convertView==null) {
				convertView = Inflater.inflate(layout,parent, false);
			}
			
			ImageView img = (ImageView) convertView.findViewById(R.id.img);
			img.setImageResource(arSrc.get(position).Icon);
			
			TextView txt = (TextView) convertView.findViewById(R.id.text);
			txt.setText(arSrc.get(position).Name);
			
			final String link = arSrc.get(position).Link;
			//Log.d("TEST",link);
			
			Button btn = (Button) convertView.findViewById(R.id.btn);
			btn.setOnClickListener(new Button.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//String str = arSrc.get(pos).Name+"를 주문합니다.";
					//Toast.makeText(maincon, str, Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(link));
					startActivity(intent);
					
				}
				
			});
			return convertView;
		}
    	
    }
}
