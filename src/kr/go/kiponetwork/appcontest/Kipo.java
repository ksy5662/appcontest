package kr.go.kiponetwork.appcontest;

import java.util.ArrayList;

import kr.go.kiponetwork.appcontest.AppAdopt.MyItem;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class Kipo extends SherlockFragment {
	
	ArrayList<MyItem> arItem;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.activity_main, container, false);
		//mList = (ListView) v.findViewById(R.id.listView1);
		//ImageView img = (ImageView) findViewBy
		//v.setBackground(@drawable/tent_10)
		return v;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		arItem = new ArrayList<MyItem>();
        MyItem mi;
        
        mi = new MyItem(R.drawable.p3, " 특허청모바일홈페이지","https://play.google.com/store/apps/details?id=kr.go.kipo.android.homepage");arItem.add(mi);
        
        mi = new MyItem(R.drawable.p2, " 특허정보검색","https://play.google.com/store/apps/details?id=kr.go.kipo.android");arItem.add(mi);
        
       
        mi = new MyItem(R.drawable.p4, " 발명카페","https://play.google.com/store/apps/details?id=kr.go.kipoportal.android");arItem.add(mi);
        
        mi = new MyItem(R.drawable.k27, " 키포아카데미","https://play.google.com/store/apps/details?id=com.fourcsoft.KipoAndroid");arItem.add(mi);
        
        mi = new MyItem(R.drawable.p5, " 지식재산권의손쉬운이용","https://play.google.com/store/apps/details?id=kr.go.kipo.easy");arItem.add(mi);
        
        mi = new MyItem(R.drawable.p6, " LTE기술길라잡이","https://play.google.com/store/apps/details?id=kr.go.kipo.lte2");arItem.add(mi);
        
        mi = new MyItem(R.drawable.p1, " 국가지식재산교육포털","https://play.google.com/store/apps/details?id=com.fourcsoft.KipaAndroid");arItem.add(mi);


        
        
        
        	
        
        MyListAdapter MyAdapter = new MyListAdapter(getActivity(),R.layout.icon_text, arItem);
        ListView MyList;
        MyList = (ListView) getView().findViewById(R.id.list);
        MyList.setAdapter(MyAdapter);
        
	}

	   
    class MyItem {
    	MyItem(int aIcon, String aName, String aLink) {
    		Icon = aIcon;
    		Name = aName;
    		Link = aLink;
    	}
    	int Icon;
    	String Name;
    	String Link;
    }
    
    class MyListAdapter extends BaseAdapter {
    	Context maincon;
    	LayoutInflater Inflater;
    	ArrayList<MyItem> arSrc;
    	int layout;
    	
    	public MyListAdapter(Context context, int alayout, ArrayList<MyItem> aarSrc) {
    		maincon = context;
    		Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		arSrc = aarSrc;
    		layout = alayout;
    	}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arSrc.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arSrc.get(position).Name;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			//final int pos = position;
			
			if (convertView==null) {
				convertView = Inflater.inflate(layout,parent, false);
			}
			
			ImageView img = (ImageView) convertView.findViewById(R.id.img);
			img.setImageResource(arSrc.get(position).Icon);
			
			TextView txt = (TextView) convertView.findViewById(R.id.text);
			txt.setText(arSrc.get(position).Name);
			
			final String link = arSrc.get(position).Link;
			//Log.d("TEST",link);
			
			Button btn = (Button) convertView.findViewById(R.id.btn);
			btn.setOnClickListener(new Button.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//String str = arSrc.get(pos).Name+"를 주문합니다.";
					//Toast.makeText(maincon, str, Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(link));
					startActivity(intent);
					
				}
				
			});
			return convertView;
		}
    	
    }
}
