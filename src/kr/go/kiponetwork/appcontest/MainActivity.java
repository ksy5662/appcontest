package kr.go.kiponetwork.appcontest;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.viewpagerindicator.TitlePageIndicator;
import com.viewpagerindicator.TitlePageIndicator.IndicatorStyle;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends SherlockFragmentActivity {


	private ViewPager mPager;
	private TitlePageIndicator mIndicator;
	private MainPagerAdapter mAdapter;
	private List<Fragment> mFragments;
	
	private static final String FRAGMENT1 = AppAdopt.class.getName();
	private static final String FRAGMENT2 = Kipo.class.getName();
	
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);
        
        
        
        mFragments = new ArrayList<Fragment>();
		mFragments.add(Fragment.instantiate(this, FRAGMENT1));
		mFragments.add(Fragment.instantiate(this, FRAGMENT2));
		
		// adapter
		mAdapter = new MainPagerAdapter(getSupportFragmentManager(), mFragments);
		
		// pager
		mPager = (ViewPager) findViewById(R.id.view_pager);
		mPager.setAdapter(mAdapter);
		mPager.setOffscreenPageLimit(10);
		
		
		// indicator
		mIndicator = (TitlePageIndicator) findViewById(R.id.title_indicator);
		mIndicator.setFooterIndicatorStyle(IndicatorStyle.Triangle);
		mIndicator.setViewPager(mPager);
        
        
        
        
        
        
    }

/*
    @Override
    public boolean onSupportedCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        
        return true;
    }
    */
 
    
}
