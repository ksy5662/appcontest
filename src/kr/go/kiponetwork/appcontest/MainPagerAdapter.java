package kr.go.kiponetwork.appcontest;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MainPagerAdapter extends FragmentPagerAdapter {
	private List<Fragment> mFragments;
	private static String[] titles = new String[] {"앱활용경진대회수상작", "특허청관련앱"};
	private int mCount = titles.length;

	public MainPagerAdapter(FragmentManager fm, List<Fragment> f) {
		super(fm);
		mFragments = f;
	}

	@Override
	public Fragment getItem(int position) {
		return mFragments.get(position);
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return titles[position];
	}
	public static void setPageTitle(String title_name, int position) {
		titles[position]=title_name;
	}
	
}
